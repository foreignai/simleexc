#!/usr/bin/env python3
import atexit
import json
import urllib.request

from flask import Flask, request, jsonify

import pymongo
from apscheduler.schedulers.background import BackgroundScheduler


# Collect JSON from URL
def json_collector():
    with urllib.request.urlopen('https://www.cbr-xml-daily.ru/daily_json.js') as answer:
        raw_data = answer.read().decode('utf-8', errors='ignore')
        data = json.loads(raw_data)
    return data


# Create prototype of struct
def record_create(data, def_code='RUB'):
    prototype = {
        'code': def_code,
        'rate': {valute: data['Valute'][valute]['Value'] / data['Valute'][valute]['Nominal'] for valute in data['Valute']}
    }

    return prototype


# Generator of records
def record_generator(prototype):
    for valute in prototype['rate']:
        rate = {}
        for local_valute in prototype['rate']:
            if local_valute != valute:
                local_rate = prototype['rate'][
                    local_valute] / prototype['rate'][valute]
                rate.update({local_valute: local_rate})
        rate.update({prototype['code']: 1 / prototype['rate'][valute]})
        new_prototype = {'code': valute, 'rate': rate}

        yield new_prototype

    yield prototype


# Base update
def update_base():
    coll.remove({})
    jsstr = json_collector()
    prtr = record_create(jsstr)
    for valute in record_generator(prtr):
        coll.save(valute)


# Init connect to MongoDB
conn = pymongo.MongoClient()
db = conn['work']
coll = db['money']


# Base update with start
update_base()


# Init web flask app
app = Flask(__name__)


# Cron init
scheduler = BackgroundScheduler()
scheduler.add_job(func=update_base, trigger="interval", hours=1)
scheduler.start()

# Shut down the scheduler when exiting the app
atexit.register(lambda: scheduler.shutdown())


# Main page route
@app.route('/')
def hello():
    return 'Main Page'


@app.route('/api/exc', methods=['GET'])
def exc_api():
    from_valute = request.args.get('from')
    to_valute = request.args.get('to')
    value_valute = request.args.get('value')
    #  Input validate of data here
    r_val = coll.find_one({'code':to_valute})['rate'][from_valute]
    e_val = r_val * int(value_valute)

    raw_answer = {
        'status': 'ok',
        'from': from_valute,
        'to': to_valute,
        'value': e_val
    }

    answer = jsonify(raw_answer)

    return answer


# Main app function
if __name__ == "__main__":
    app.run(host='0.0.0.0', port=80)
    